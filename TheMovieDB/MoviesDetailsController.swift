//
//  MoviesDetailsController.swift
//  TheMovieDB
//
//  Created by Matran, Bogdan on 28/06/2018.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import UIKit

class MoviesDetailsController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var imageSpacingView: UIView!
    @IBOutlet weak var titleSpacingView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearDurationGenreLabel: UILabel!
    @IBOutlet weak var launchDate: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var addToWatchlistButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var greenImageView: UIImageView!
    
    var movie: MovieIDB?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        decorate()
    }
    
    func decorate() {
        titleSpacingView.frame.size.height = 0.5
        imageSpacingView.frame.size.height = 0.5
        scoreLabel.text = "\(movie?.vote_average ?? 0)"
        descriptionTextView.isEditable = false
        titleLabel.text = movie?.title
        yearDurationGenreLabel.text = movie?.original_title
        launchDate.text = movie?.release_date
        guard let url = movie?.poster_path else {
            return
        }
        movieImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w500/" + url)
        descriptionTextView.text = movie?.overview
        ratingLabel.text = "\(movie?.vote_count ?? 0)"
        
    }
}
