//
//  Audiobook.swift
//
//  Created by Isabela Claudia GROZA on 6/25/18.
//  Copyright © 2018 Pentalog. All rights reserved.
//

import UIKit

struct MovieIDB: Decodable {
    var vote_count: Int?
    var id: Int?
    var video: Bool?
    var vote_average: Double?
    var title: String?
    var popularity: Double?
    var poster_path: String?
    var original_language: String?
    var original_title: String?
    var genre_ids: [Int]?
    var backdrop_path: String?
    var adult: Bool?
    var overview: String?
    var release_date: String?
}

