//
//  DataStore.swift
//
//  Created by Isabela Claudia GROZA on 6/25/18.
//  Copyright © 2018 Pentalog. All rights reserved.
//

import Foundation
import UIKit

class DataStore {
    
    /// The instance of DataSore object
    static let sharedInstance = DataStore()
    fileprivate init() {}
    var audiobooks = [MovieIDB]()
    var images: [String: UIImage] = [:]
    
    func getBooks(completion: @escaping ([MovieIDB]) -> Void) {
        let session = URLSession.shared
        let baseUrl = "https://api.themoviedb.org/3/movie/popular"
        let languageValue = "language"
        let language = "en"
        let api_key = "api_key"
        let api_keyValue = "2652f9a822573315e3a1f18a6d031384"
        let page = "page"
        let pageValue = "1"
        let urlString = baseUrl + "?" + api_key + "=" + api_keyValue + "&" + language + "=" + languageValue + "&" + page + "=" + pageValue
        let url = URL(string: urlString)!
        let task = session.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let error = error {
                print(error)
                completion([])
                return
            }
            guard let data = data else {
                print("No data!!!")
                completion([])
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []),
                let jsonData = json as? [String: Any]
                else {
                    completion([])
                    return
            }
            guard let results = jsonData["results"] as? [[String: Any]] else {
                completion([])
                return
            }
            
            let decoder = JSONDecoder()
            guard let encodedData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted),
                let objects = try? decoder.decode([MovieIDB].self, from: encodedData)
            else {
                completion([])
                return
            }
            self.audiobooks = objects
            completion(self.audiobooks)
        })
        task.resume()
    }
    
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DataStore.sharedInstance.images[url.absoluteString] = image
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        if let image = DataStore.sharedInstance.images[link] {
            self.image = image
            return
        }
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
