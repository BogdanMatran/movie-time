//
//  AppDelegate.swift
//  CollectionViewBlog
//
//  Created by Isabela Claudia GROZA on 6/25/18.
//  Copyright © 2018 Isabela Claudia GROZA. All rights reserved.
//


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Your code goes here
        return true
    }

}

