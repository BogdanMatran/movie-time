//
//  ViewController.swift
//  CollectionViewBlog
//
//  Created by Isabela Claudia GROZA on 6/25/18.
//  Copyright © 2018 Isabela Claudia GROZA. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    let collectionViewMargins: CGFloat = 5
    let collectionViewCellHeight: CGFloat = 180
    let numberOfCellsPerRow: CGFloat = 2
    
    var dataSource: [MovieIDB]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        navigationItem.title = "Movies"
        registerNibs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DataStore.sharedInstance.getBooks { [weak self] (moviesArray) in
            DispatchQueue.main.async {
                self?.dataSource = moviesArray
                self?.collectionView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let moviesCount = dataSource?.count else {
            return 0
        }
        return moviesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MoviesCollectionViewCell.identifier(), for: indexPath) as? MoviesCollectionViewCell else {
            return UICollectionViewCell()
        }
        guard let currentMovie = dataSource?[indexPath.row] else { return UICollectionViewCell() }
        cell.decorate(with: currentMovie)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let movie = dataSource?[indexPath.row] else {
            return
        }
        self.performSegue(withIdentifier: "transitionToDetails", sender: movie)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.bounds.size.width/numberOfCellsPerRow - collectionViewMargins
        return CGSize(width: screenWidth, height: collectionViewCellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    private func registerNibs() {
        collectionView.register(UINib(nibName: String(describing: MoviesCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: MoviesCollectionViewCell.identifier())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transitionToDetails" {
            guard let detailsVC = segue.destination as? MoviesDetailsController else { return }
            if let movie = sender as? MovieIDB {
                detailsVC.movie = movie
            }
        }
    }
}
