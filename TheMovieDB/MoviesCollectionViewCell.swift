//
//  MoviesCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Matran, Bogdan on 26/06/2018.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var metascoreLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func identifier() -> String {
        return "MoviesCollectionViewCell"
    }
    
    func decorate(with movie: MovieIDB) {
        titleLabel.text = movie.title
        let metascore = movie.vote_average
        metascoreLabel.text = "\(Int(metascore!))"
        let url = movie.poster_path
        let link = "https://image.tmdb.org/t/p/w500/" + url!
        movieImage.downloadedFrom(link: link)
    }
}
